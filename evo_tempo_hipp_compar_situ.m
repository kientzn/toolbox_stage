%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% PLOTS TEMPORAL EVOLUTION ALONG HIPPODROME TRAJECTORY %%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%% OF BIOMASSE MODELED & IN SITU %%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% N. KIENTZ, 2022 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
tic
clear all;
close all;

%% Data
load('outputs/pico_struc')
load('outputs/micro_struc')
load('outputs/zoo_Lagrangian')
time_mod = zoo_all.time;
dataCYTONEW = dlmread('data_CYTO_NEW.txt');
dataCYTONEW = dataCYTONEW(407:511,:);

%% Get modelled data corresponding to the hippodrome
for i = 1:105
    time_situ(i) = datenum(dataCYTONEW(i,1),dataCYTONEW(i,2),dataCYTONEW(i,3),dataCYTONEW(i,4),dataCYTONEW(i,5),dataCYTONEW(i,6));
    [d, it ] = min( abs( time_situ(i)-time_mod ) );        
    time_mod_ok(i)=it;
    [d, ix ] = min( abs( pico.lon_situ(i)-pico.lon_mod ) );        
    lon_mod_ok(i)=ix;
    [d, iy ] = min( abs( pico.lat_situ(i)-pico.lat_mod ) );        
    lat_mod_ok(i)=iy;
end

%% Mean in situ data in each modelled grid cell in function of time
n=0;
a = 1;
for i = 2:length(lat_mod_ok)
    if lat_mod_ok(i)==lat_mod_ok(i-1) && time_mod_ok(i)== time_mod_ok(i-1)
        n=n+1;
        list(n)=i;
        gr_pico(n)=mean(pico.biom_situ(a:i),'omitnan');
        err_pico(n)=std(pico.biom_situ(a:i),'omitnan');
        gr_micro(n)=mean(micro.biom_situ(a:i),'omitnan');
        err_micro(n)=std(micro.biom_situ(a:i),'omitnan');
        a = a+1;
    else
        n=n+1;
        list(n)=i;
        gr_pico(n)=pico.biom_situ(i);
        err_pico(n)=0;
        gr_micro(n)=micro.biom_situ(i);
        err_micro(n)=0;
        a = a+1;
    end
end

%% Reshape model outputs and get model values corresponding to the in situ measurements
%
n_array = 3;
lat_edge = min(zoo_all.lat2D)+(0.02*n_array)/2:0.02*n_array:max(zoo_all.lat2D)+(0.02*n_array)/2;
lon_edge = min(zoo_all.lon2D)+(0.02*n_array)/2:0.02*n_array:max(zoo_all.lon2D)+(0.02*n_array)/2;
for it = 1:size(zoo_all.lon2D,2)
    Data_micro = zoo_all.P_big(:,it);%1:it
    meanData_micro(:,:,it) = histcn([zoo_all.lat2D(:,time_mod_ok(it)) zoo_all.lon2D(:,time_mod_ok(it))], lat_edge, lon_edge, 'AccumData', Data_micro, 'Fun', @mean);   
    Data_pico = zoo_all.P_small(:,it);
    meanData_pico(:,:,it) = histcn([zoo_all.lat2D(:,time_mod_ok(it)) zoo_all.lon2D(:,time_mod_ok(it))], lat_edge, lon_edge, 'AccumData', Data_pico, 'Fun', @mean);   
end

%%
for i = 1:length(gr_pico)
    gr_mod_micro(i) = meanData_micro(lat_mod_ok(list(i)),lon_mod_ok(list(i)),time_mod_ok(list(i)));
    gr_mod_pico(i) = meanData_pico(lat_mod_ok(list(i)),lon_mod_ok(list(i)),time_mod_ok(list(i)));
    time_ok(i) = zoo_all.time(time_mod_ok(list(i)));
end


%% Plot evolution of biomasse over time
gr_mod_micro(gr_mod_micro==0)=NaN;
figure('DefaultAxesFontSize',22), hold on
plot(time_situ(2:end),gr_mod_micro,'.-','LineWidth',2)
errorbar(time_situ(2:end),gr_micro,err_micro,'-o')
legend('micro modélisé','micro in situ')
datetick('x','dd-mm')
xlabel('Temps')
ylabel('Biomasse (mmol C.m⁻³)')
grid('on')
grid('minor')

gr_mod_pico(gr_mod_pico==0)=NaN;
figure('DefaultAxesFontSize',22), hold on
plot(time_situ(2:end),gr_mod_pico,'.-','LineWidth',2)
errorbar(time_situ(2:end),gr_pico,err_pico,'-o')
legend('pico modélisé','pico in situ')
datetick('x','dd-mm')
xlabel('Temps')
ylabel('Biomasse (mmol C.m⁻³)')
grid('on')
grid('minor')
toc