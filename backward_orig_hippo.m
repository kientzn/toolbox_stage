%% START_GA_TOOLBOX: examples to run the model
% Reference: Messi�, M., & Chavez, F. P. (2017). Nutrient supply, surface currents, and plankton dynamics predict zooplankton hotspots 
%					in coastal upwelling systems. Geophysical Research Letters, 44(17), 8979-8986, https://doi.org/10.1002/2017GL074322

tic

run('transfo_currents.m');
run('chl_map.m');
run('sst_map.m');
run('abundance_map.m');
run('Abundance_relations.m');
run('relation_SST_T_tsg.m');
run('test_ini_map.m');
% --------------------------------------------- Set up ----------------------------------------------- %%
disp('start_GA_toolbox_test : ...');
% Set directory where Ariane is installed (used in most functions)
% There must be a directory "currents_data" inside dir_ariane_global where currents netcdf files are saved (see ga_write_ariane_currents)
global dir_ariane_global
dir_ariane_global='Ariane_workplace/';

% Set directory where outputs will be saved (used in ga_full_GArun)
global dir_output_global
dir_output_global='outputs/';

% Note - m_map can be used to visualize output maps (https://www.eoas.ubc.ca/~rich/map.html).

%%%%%%%% GENERATE 1 PARTICULE FOR EACH GRID MESH %%%%%%%%%%%%%%%%%%%%%%%%
xvalue = 2.75:0.1:3.75;
yvalue = 38:0.1:39;
[X,Y] = meshgrid(xvalue, yvalue);
point_1 = [X(:) Y(:)];


positions=ga_advection_ariane(point_1,'PROTEVS_','dt',0.1,'time0',time(120)+13,'nbdays_advec',13,'backwards');
time2D=repmat(positions.time',size(point_1,1),1);

% positions_f=ga_advection_ariane(point_1,'PROTEVS_','dt',0.1,'time0',time(120),'nbdays_advec',13);
% time2D_f=repmat(positions_f.time',size(point_1,1),1);

figure('DefaultAxesFontSize',22)
m_proj('mercator','lon',[1 6],'lat',[37 39.5]);
m_scatter(positions.lon2D(positions.lat2D(:,end)<=38.6,:),positions.lat2D(positions.lat2D(:,end)<=38.6,:),10,'b','filled');
hold on
m_scatter(positions.lon2D(positions.lat2D(:,end)>38.6,:),positions.lat2D(positions.lat2D(:,end)>38.6,:),10,'r','filled');
hold on
m_plot(positions.lon2D(:,1),positions.lat2D(:,1),'k*');
hold on
m_plot(positions.lon2D(:,end),positions.lat2D(:,end),'c.','MarkerSize',20);
% %legend('trajectoires des particules de la partie Sud du front','trajectoires des particules de la partie Nord du front','positions des particules au moment des mesures de l hippodrome','origines des particules','Location','best') 
% m_scatter(positions.lon2D(:),positions.lat2D(:),35,time2D(:))
% colorbar
% cbdate
m_usercoast('gumby','patch','w');
m_grid('box','fancy','linestyle','-','gridcolor','none','backcolor','none');

toc