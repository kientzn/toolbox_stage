%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% ANALYSE T, CHL & NUT OVER A TRAJECTORY %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% N. KIENTZ 2022 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
close all;

addpath('PHOSPHATE');

%%% choose study case:
% c = 1 for analyse along particule trajectory with real value
% c = 2 for 1D study with fixed nutrients and time

c = 2;
load('inputs/positions')
if c ==1
%extract SST and CHL from particle positions
%find the matching pixel
p_nb = 7180; %particule number to extract trajectory
%p_nb = 70;
positions_lat = positions.lat2D(p_nb,:);
positions_lon = positions.lon2D(p_nb,:);
time = positions.time;

% iter_lat = [];
% iter_lat_chl = [];
% for i = 1:length(positions_lat)
% iter_lat(i) = floor((positions_lat(i)-min(lat))/0.01);
% iter_lat_chl(i) = floor((positions_lat(i)-min(lat))/0.01);
% end
% iter_lon = [];
% iter_lon_chl = [];
% for i = 1:length(positions_lon)
% iter_lon(i) = floor((positions_lon(i)-min(lon))/0.01);
% iter_lon_chl(i) = floor((positions_lon(i)-min(lon))/0.01);
% end
%SST_pos = [];
% CHL_pos = [];
% for i = 1:length(zoo_all.time)
%     %date = dataCYTONEW(i,3);
%     %SST_pos(i) = sst_MUR(iter_lon(i),iter_lat(i),119+floor(i/10)+1);
%     CHL_pos(i) = chl_CMEMS(iter_lon_chl(i),iter_lat_chl(i),119+floor(i/5)+1);
% end
%save('SST_pos');

load('inputs/Nsupply_PROTEVS')
NUT_pos = [];
lon_index_nut = [];
lat_index_nut = [];
for it = 1:length(positions.time)
    time = floor(it/10)+1;
 [d, ix ] = min( abs( Nsupply_PROTEVS.lon-positions_lon(it) ) );        
        lon_index_nut(it)=ix;%indice des latitudes
 [d, iy ] = min( abs( Nsupply_PROTEVS.lat-positions_lat(it) ) );        
        lat_index_nut(it)=iy;%indice des latitudes
NUT_pos(it)=Nsupply_PROTEVS.Nsupply(ix,iy,time);
end
% NUT_pos = ones(length(positions.time),1)*0.0038299; %fixed value at the
% mean of PEACETIME measurements
%%
%ga_model_2P2Z_fromPsupply_Med(NUT_pos,'time',positions.time,'plot',SST_pos)
load('outputs/zoo_Lagrangian')
load('outputs/Nsupply_traj')
load('outputs/SST_traj')

figure('DefaultAxesFontSize',22), hold on
ax = gca;
ax.FontSize = 16; 
    m_proj('mercator','lon',[min(zoo_all.lon_ini) max(zoo_all.lon_ini)],'lat',[min(zoo_all.lat_ini) max(zoo_all.lat_ini)]);
    m_scatter(zoo_all.lon2D(p_nb,:),zoo_all.lat2D(p_nb,:),20,zoo_all.time,'filled')
    colorbar;
    cbdate
    hold on
    m_usercoast('gumby','patch','w'); 
    m_grid('box','fancy','linestyle','-','gridcolor','w','backcolor','none');
	xlabel('Longitude'), ylabel('Latitude')
	title('Particule trajectory initialized on April 30, 2018')


%%% Evolution of SST, CHL (sat & model) and Nutrients
figure('DefaultAxesFontSize',22); 
subplot(3,1,1)
plot(zoo_all.time,SST_traj.SST_part(p_nb,:),'b.-') 
ylabel('SST (°C)','Fontsize',14)
datetick('x','dd','keeplimits')
% grid('on')
% grid('minor')

subplot(3,1,2)
plot(zoo_all.time,zoo_all.P_small(p_nb,:),'k.-')
hold on
plot(zoo_all.time,zoo_all.Z_small(p_nb,:),'r.-')
hold on
plot(zoo_all.time,zoo_all.P_big(p_nb,:),'b.-')
hold on
plot(zoo_all.time,zoo_all.Z_big(p_nb,:),'g.-')

ylabel('Biomasse','Fontsize',14)
datetick('x','dd','keeplimits')
legend('Psmall','Zsmall','Pbig','Zbig')
% plot(zoo_all.time,CHL_pos.*88.4/100,'LineWidth',2) 
% hold on
% plot(zoo_all.time+(positions.time),zoo_all.Chl(p_nb,:),'LineWidth',2);
% legend('CHL CMEMS','CHL equivalent to model biomasse','Location','best')
% ylabel('CHL (mg.m⁻³)','Fontsize',14)
% datetick('x','dd','keeplimits')
% % grid('on')
% % grid('minor')

subplot(3,1,3)
plot(zoo_all.time,Nsupply_traj.Nsupply_part(p_nb,:),'b') 
ylabel('Phosphates (mmol C.m^{-3}) ','Fontsize',14)
datetick('x','dd','keeplimits')
% grid('on')
% grid('minor')
% 
% subplot(4,1,4)
% ax = gca;
% ax.FontSize = 16;
% plot(datenum(1950,01,01)+(positions.time),PHOS_pos,'b.-')
% ylabel('Phosphate (mmol P.m^{-3}) ','Fontsize',14)
% datetick('x','dd','keeplimits')
% grid('on')
% grid('minor')
%title('Evolution of different parameters along the particule trajectory')

%%% Comparison of growth rate methods
% delta_sst = ones(length(positions.time),1);
% delta_sst(1)=0;
% partie_coef=[];
% umax_T = [0];
% coef_T = [0];
% for i = 2:length(positions.time)
%     delta_sst(i,1) = SST_pos(floor(i/10)+2)-SST_pos(floor(i/10)+1);
%     %delta_sst(i,1) = SST_pos(floor(i/10)+1);
%     coef_T(i) = (24.51*(delta_sst(i))^4 + 53.51*(delta_sst(i))^3 + 24.56*(delta_sst(i))^2 + 260.3*delta_sst(i));
%     partie_coef(i)=(1.9872+coef_T(i))/abs(1.9872-coef_T(i));
%     umax_T(i) = output.u_small(i)*(1.9872+coef_T(i))/1.9872;
%     %umax_T(i) = ((PHOS_pos(i)*5)/60 + 5*PHOS_pos(i)*1.9872*(24.51*(delta_sst(i))^4 + 53.51*(delta_sst(i))^3 + 24.56*(delta_sst(i))^2 + 260.3*delta_sst(i) + 1022)*pente_t_tsg);
% end


% figure('DefaultAxesFontSize',22);
% legend
% a=plot(zoo_all.time,zoo_all.,'-','DisplayName','modèle');
% hold on
% b=plot(datenum(1950,01,01)+positions.time,0.81*exp(0.0631*SST_pos),'-','DisplayName','Bissinger 2008'); %Bissinger (2008)
% hold on
% c=plot(datenum(1950,01,01)+positions.time,0.59*exp(0.0633*SST_pos),'-','DisplayName','Eppley 1972'); %Eppley (1972)
% hold on
% d=plot(datenum(1950,01,01)+positions.time,ones(size(positions.time,1))*1.9872,'-','DisplayName','umax Baklouti 2021');
% datetick('x','dd-mm-yyyy','keeplimits')
% grid('on')
% grid('minor')
% 
% %legend('umax modèle','umax fct T','umax max Baklouti 2021','Location','best') %Bissinger et al. (2008)
% %legend('umax modèle','umax modèle fct T','umax max Baklouti 2021','Location','best') %Bissinger et al. (2008)
% title('Comparison of growth rate methods along the particule trajectory')
% hold off
end


if c == 2 
time = 1:0.1:600; 
NUT_pos = ones(6000,1)*10E-3;
ga_model_2P2Z_fromPsupply_Med(NUT_pos,'time',time,'plot')

%load('output','P_small','P_big','Z_small','Z_big','u_big','u_small')
load('outputs/output')

%min_chl = ones(length(time))*(min(min(min(chl_CMEMS(:,:,120:150))))*(10/100));
%mean_chl = ones(length(time))*(mean(mean(mean(chl_CMEMS(:,:,120:150),'omitnan'),'omitnan'))*10/100);   

figure('DefaultAxesFontSize',22), hold on
plot(time,output.Chl,'LineWidth',2);
%plot(time,mean_chl,'LineWidth',2);
%plot(time, min_chl,'LineWidth',2);
%legend('Chl modèle','Chl CMEMS moyenne','Chl CMEMS min');
title('Chl équivalente à la biomasse modélisée (mg.m^{-3})')

end


