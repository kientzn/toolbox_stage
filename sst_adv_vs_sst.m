%%% Comparison between advected SST and SST
%%% N. KIENTZ, 2022 %%%%%%%%%%%%%%%%%%%%%%%
close all;
clear all;

tic

run('transfo_currents.m');
run('chl_map.m');
run('sst_map.m');
run('abundance_map.m');
run('Abundance_relations.m');
run('relation_SST_T_tsg.m');
run('test_ini_map.m');

% --------------------------------------------- Set up ----------------------------------------------- %%
disp('start_GA_toolbox_test : ...');
% Set directory where Ariane is installed (used in most functions)
% There must be a directory "currents_data" inside dir_ariane_global where currents netcdf files are saved (see ga_write_ariane_currents)
global dir_ariane_global
dir_ariane_global='Ariane_workplace/';

% Set directory where outputs will be saved (used in ga_full_GArun)
global dir_output_global
dir_output_global='outputs/';

% Note - m_map can be used to visualize output maps (https://www.eoas.ubc.ca/~rich/map.html).

%%%%%%%% GENERATE 1 PARTICULE FOR EACH GRID MESH %%%%%%%%%%%%%%%%%%%%%%%%
xvalue = 0:0.01:7;
yvalue = 36:0.01:40;
[X,Y] = meshgrid(xvalue, yvalue);
point_1 = [X(:) Y(:)];
% 
SST_MUR=struct();
SST_MUR.sst = sst_MUR;
SST_MUR.lon_sst = lon_MUR;
SST_MUR.lat_sst = lat_MUR;
SST_MUR.time_sst = datenum(2018,1,1):datenum(2018,12,31);
% 
iok = ~isnan(mat_micro);
mat_positions_ini=[X(iok) Y(iok)];
% %save('inputs/mat_positions_ini','mat_positions_ini')
positions=ga_advection_ariane(mat_positions_ini,'PROTEVS_','dt',1,'time0',time(120),'nbdays_advec',20,'backward');

time2D=repmat(positions.time',size(mat_positions_ini,1),1);

sst_matrix = reshape(mat_sst(:,:),[size(mat_sst,2)*size(mat_sst,1),1]);
for it = 1:length(time2D(1,:))-1
    sst_matrix= [sst_matrix reshape(mat_sst(:,:),[size(mat_sst,2)*size(mat_sst,1),1])];
end

n_array = 2;
lat_edge = 36+(0.01*n_array)/2:0.01*n_array:40+(0.01*n_array)/2;
lon_edge = 0+(0.01*n_array)/2:0.01*n_array:7+(0.01*n_array)/2;
Data = sst_matrix(:,1);
meanData_sst = nan(250,350,21);
meanData = histcn([positions.lat_ini(:) positions.lon_ini(:)], lat_edge(1:end), lon_edge(1:end), 'AccumData', Data, 'Fun', @mean);
meanData_sst(1:size(meanData,1),1:size(meanData,2),1)=meanData;
for it = 2:21
    Data = sst_matrix(:,it);
    meanData = histcn([positions.lat2D(:,it) positions.lon2D(:,it)], lat_edge(1:end), lon_edge(1:end), 'AccumData', Data, 'Fun', @mean);
    meanData_sst(1:size(meanData,1),1:size(meanData,2),it)=meanData;   
end

for it = 1:21
for ix = 1:length(lon_edge)
    for iy = 1:length(lat_edge)
        if lon_edge(ix) <= 3.5 && lon_edge(ix) >= 2.75 && lat_edge(iy) <= 39 && lat_edge(iy)>= 38
            sst_ok(ix,iy,it) = meanData_sst(ix,iy,it);
        end
    end
end
for i = 1:length(SST_MUR.lon_sst)
    for j = 1:length(SST_MUR.lat_sst)
        if SST_MUR.lon_sst(i) <= 3.5 && SST_MUR.lon_sst(i) >= 2.75 && SST_MUR.lat_sst(j) <= 39 && SST_MUR.lat_sst(j)>= 38
            sst_mur_ok(i,j,it)=SST_MUR.sst(i,j,it+119);
        end
    end
end
sst_mur_ok(sst_mur_ok==0)=NaN;
sst_ok(sst_ok==0)=NaN;
mur_mean(it)=mean(mean(sst_mur_ok(:,:,it),'omitnan'),'omitnan');
err_mur(it)=std(std(sst_mur_ok(:,:,it),'omitnan'),'omitnan');
sst_mean(it)=mean(mean(sst_ok(:,:,it),'omitnan'),'omitnan');
err_sst(it)=std(std(sst_ok(:,:,it),'omitnan'),'omitnan');
end
mur_mean_mean = mean(mur_mean);
sst_mean_mean = mean(sst_mean);

figure('DefaultAxesFontSize',22), hold on
errorbar(datenum(2018,04,30):datenum(2018,04,30)+20,mur_mean-mur_mean_mean+sst_mean_mean,err_mur)
errorbar(datenum(2018,04,30):datenum(2018,04,30)+20,sst_mean-sst_mean_mean+mur_mean_mean,err_sst)
% errorbar(datenum(2018,04,30):datenum(2018,04,30)+20,mur_mean,err_mur)
% errorbar(datenum(2018,04,30):datenum(2018,04,30)+20,sst_mean,err_sst)
legend('SST satellite','SST modélisée')
datetick('x','dd-mm')
xlabel('Temps')
ylabel('SST (°C)')
grid('on')
grid('minor')

toc

