%%%%%%%%%%%%%%%%%%%%%%%%
%%%%  READ ME  %%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%

%%%%%%%% PERIODS %%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%% amplitudes temporelles choisies pour matcher les périodes temporelles des fichiers de chl

w1 = 2018/04/28 to 2018/05/05
w2 = 2018/05/03 to 2018/05/10
w3 = 2018/05/09 to 2018/05/16
w4 = 2018/05/15 to 2018/05/21

%%%%%%%% STANDARD DEVIATION %%%%%%%%%%%%%%%%%%
"SD_chl.png" : SD pour l'ensemble de la période 2018/04/28 to 2018/05/21 + nombre de mesures permettant de moyenner
"SD_chl_w1.png" : idem mais que pour période w1
"SDzoom_chl.png" : SD avec colorbar bornée à [0, 0.2] pour mettre en lumière les zones relativement plus variables dans celles présentant une faible variabilité
...

%%%%%%%% ABUNDANCE TRAJECTORIES %%%%%%%%%%%%%%
"pico1_w1.png" : évolution spatio temporelle de l'abondance du groupe pico1 sur la période w1
...

"pico1_abundance_start_20180420.png" : évo. spatio-temporelle sur l'ensemble de la période 2018/04/28 to 2018/05/21
...

%%%%%%%% STANDARD DEVIATION %%%%%%%%%%%%%%%%%%
%SD = sqrt((sum(|x-µ|**2))/N)

