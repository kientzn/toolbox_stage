# Toolbox stage

### Description

This toolbox contains the Matlab programs necessary to the work coupled with Ariane2D (https://github.com/messiem/toolbox_GrowthAdvection) of my MSc internship.
These scripts have been build to answer specific questions with linked data, but still relatively easy adaptable to other data, areas and parameters.
* * *
### USER GUIDE
**abundance_map.m** import abundance data and plot them (map of each measurements for every groups, map of abundance patchs linked to chl distribution, map of each measurement with chl contour). 

**Abundance_relations.m** plots relation between abundance and different parameters (S, T, SST, T/S, CHL, PAR) and their linear correlation and R² associated.

**Analyse_for_model.m** makes analyse (biomass, comparison between CHL and nutrient producs, comparison of different growth rate calculation methods, temperature, nutrients) with fixed time and nutrients & temperature inputs or with real nutrients and temperature inputs along a particle trajectory

**chl_map.m** import CHL data and plot them. Also averages them over the period and calculates and maps SD of the data and the number of measurements for the chosen period.

**correl_ab.m** plots correlations between modeled and in situ biomasses.

**evo_tempo_hipp_compar_situ.m** plots temporal evolution of biomasses (modeled & in situ) along hippodrome trajectory.

**fitting_micro.m** and **fitting_pico2.m** are the functions used to find the relation between abundance and temperature from thermosalinograph.

**ga_advection_ariane.m** computes 2D surface current trajectories using Ariane (Messié et al., 2020).

**ga_concatenation.m** takes the daily Lagrangian outputs saved into the output directory (as calculated by ga_growthadvection) (M. Messié, 2021).

**ga_full_GArun.m** run the plancton model alongside Lagrangian trajectories (Adapted from Messié et al., 2021).

**ga_growth_advection.m** runs the plankton model alongside Lagrangian trajectories for a given initialization (Nsupply, Temperature) for every particle at each time step.

**ga_model_2P2Z_fromNsupply_Med.m** is the biogeochemical model based on N supply (Adapted from Messié & Chavez, 2017, Messié et al., 2021).

**ga_model_2P2Z_fromP[...]_Med.m** is the biogeochemical model based on P supply (limiting element in the area of insterest) and depending title on temperature with Eppley (1970) or Bissinger (2006) relation (Adapted from Messié & Chavez, 2017, Messié et al., 2021).

**ga_write_ariane_currents.m** writes current netcdf files in Ariane format (M. Messié, 2021).

**PO4_flux_estimation.m** is the script used to estimate in space and in time the PO4 flux at the base of the MLD following the method described in Pulido-Villena et al. (2021).

**relation_SST_T_tsg.m** plots relation between in situ temperature and several products.

**sst_adv_vs_sst.m** comparison between advected SST and SST.

**sst_map.m** import SST data and plot them. Also averages them over the period and calculates and maps standard deviation of the data and the number of measurements for the chosen period.

**start_GA_toolbox_test.m** compute positions of particles (after generating them) using Ariane. Also compute abundance distribution over time by assign abundance related to chl distribution (cf. **chl_map.m** and **abundance_map.m**). Also plots map of averaged abundances after time. 

**test_ini_map.m** makes initial maps of phytoplankton groups and Nutrients by extrapolating from found relations.

**transfo_currents.m** transforms the netcdf current file into an Ariane2D compatible format matrix.





### Contact

nathankientz1998@gmail.com

Do not hesitate to contact me if you if you have any questions or some improvment notes! 
