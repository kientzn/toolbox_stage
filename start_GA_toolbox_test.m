%% START_GA_TOOLBOX: examples to run the model
% Reference: Messi�, M., & Chavez, F. P. (2017). Nutrient supply, surface currents, and plankton dynamics predict zooplankton hotspots 
%					in coastal upwelling systems. Geophysical Research Letters, 44(17), 8979-8986, https://doi.org/10.1002/2017GL074322

tic

run('transfo_currents.m');
run('chl_map.m');
run('sst_map.m');
run('abundance_map.m');
run('Abundance_relations.m');
run('relation_SST_T_tsg.m');
run('test_ini_map.m');
% --------------------------------------------- Set up ----------------------------------------------- %%
disp('start_GA_toolbox_test : ...');
% Set directory where Ariane is installed (used in most functions)
% There must be a directory "currents_data" inside dir_ariane_global where currents netcdf files are saved (see ga_write_ariane_currents)
global dir_ariane_global
dir_ariane_global='Ariane_workplace/';

% Set directory where outputs will be saved (used in ga_full_GArun)
global dir_output_global
dir_output_global='outputs/';

load('inputs/curr_struc')
% Note - m_map can be used to visualize output maps (https://www.eoas.ubc.ca/~rich/map.html).

%%%%%%%%% GENERATE 1 PARTICULE FOR EACH GRID MESH %%%%%%%%%%%%%%%%%%%%%%%%
xvalue = min(lon_MUR):0.5:max(lon_MUR);
yvalue = min(lat_MUR):0.5:max(lat_MUR);
[X,Y] = meshgrid(xvalue, yvalue);
point_1 = [X(:) Y(:)];
% 
SST_MUR=struct();
SST_MUR.sst = sst_MUR;
SST_MUR.lon_sst = lon_sst_MUR;
SST_MUR.lat_sst = lat_sst_MUR;
SST_MUR.time_sst = datenum(2018,1,1):datenum(2018,12,31);
save('inputs/SST_MUR','SST_MUR')
% 
save('inputs/time','time')
%If ga_advection_ariane does not work, try first to run "ariane" from a terminal inside Ariane_workplace.
iok = ~isnan(mat_micro);
mat_positions_ini=[X(:) Y(:)];
save('inputs/mat_positions_ini','mat_positions_ini')
positions=ga_advection_ariane(point_1,'PROTEVS_','dt',0.5,'time0',time(120),'nbdays_advec',12);
save('inputs/positions','positions');
time2D=repmat(positions.time',size(mat_positions_ini,1),1);
% 
% %% ---- Compute abundances over time using Ariane ---- %%
% 
% % If ga_advection_ariane does not work, try first to run "ariane" from a terminal inside Ariane_workplace.
% 
% %%% Micro extrapolated
% abundance_matrix_micro_extp = reshape(mat_micro,[size(mat_micro,1)*size(mat_micro,2),1]);
% for it = 1:length(time2D(1,:))-1
%     abundance_matrix_micro_extp = [abundance_matrix_micro_extp reshape(mat_micro,[size(mat_micro,1)*size(mat_micro,2),1])];
% end
% 
%  
% % %% Pico extrapolated
% mat_pico = mat_pico(iok);
% abundance_matrix_pico_extp = reshape(mat_pico,[size(mat_pico,1)*size(mat_pico,2),1]);
% for it = 1:length(time2D(1,:))-1
%     abundance_matrix_pico_extp = [abundance_matrix_pico_extp reshape(mat_pico,[size(mat_pico,1)*size(mat_pico,2),1])];
% end
% 
% %% SST advected
% tok = ~isnan(mat_sst);
% mat_sst = mat_sst(tok);
% sst_matrix = reshape(mat_sst(:,:),[size(mat_sst,1)*size(mat_sst,2),1]);
% for it = 1:length(time2D(1,:))-1
%     sst_matrix= [sst_matrix reshape(mat_sst(:,:),[size(mat_sst,1)*size(mat_sst,2),1])];
% end
%     
% %   %% Particles advected
% pok = ~isnan(mat_sst);
% % pok = pok.*(positions.lon2D(:,1));
% particles_matrix = reshape(pok(:,:),[size(pok,1)*size(pok,2),1]);
% for it = 1:length(time2D(1,:))-1
%     particles_matrix = [particles_matrix reshape(pok(:,:),[size(pok,1)*size(pok,2),1])];
% end  

% Averaged abundance
% Compute the mean on rectangular patch from scattered data
% Datasize = length(lat_MUR)*length(lon_MUR);
% Lat = positions.lat2D(:,1);
% Lon = positions.lon2D(:,1);
% 
% %Data = sst_matrix(:,:);
% %Data = particles_matrix(:,:).*(positions.lat2D(:,end)-positions.lat2D(:,1)); %delta lat or lon between final positions and initial positions of particles
% Data = abundance_matrix_pico_extp(:,end);
% n_array = 2;
% lat_edge = min(lat_MUR):0.01*n_array:max(lat_MUR);
% lon_edge = min(lon_MUR):0.01*n_array:max(lon_MUR);
% meanData = histcn([Lat(:) Lon(:)], lat_edge, lon_edge, 'AccumData', Data, 'Fun', @mean);
% NaN_column = NaN(size(meanData,1),1);
% meanData = [NaN_column meanData];
% meanData(size(meanData,1)+1,:) = NaN;
% meanData(meanData==0)=NaN;
% 
% figure('DefaultAxesFontSize',22)
% m_proj('mercator','lon',[0 7],'lat',[36 40]);
% m_pcolor(lon_edge(1:end),lat_edge(1:end),meanData)
% shading flat
% hold on
% %m_scatter(lon_abundance(118:510),lat_abundance(118:510),30,dataCYTONEW(118:510,10),'filled')
% %m_quiver(X_curr,Y_curr,mean(u0(:,:,131:133),3),mean(v0(:,:,131:133),3),3,'r');
% m_usercoast('gumby','patch','w');
% m_grid('box','fancy','linestyle','-','gridcolor','none','backcolor','none');
% hold on
% cbar = colorbar;
% colorTitleHandle = get(cbar,'Title');
% titleString = ({'\Delta lat (°N)'});
% set(colorTitleHandle ,'String',titleString);


% %% --------------------------- Compute one daily run starting on May 1st, 2008 --------------------------- %%
% 
% %Load inputs and set run date
% load('inputs/Nsupply_PROTEVS.mat')						% load the Nsupply forcing for the 2008 season
% options_plankton_model={'gmax_big',0.432,'eZ',0.1,'mZ',0.005};	% krill parameterization
% name_curr='PROTEVS_bio';											% using currents toolbox_* into Ariane_workplace/currents_data
% time0=datenum(2018,4,30);	
% 
% % Construct init structure
% init=struct();
% init.Nsupply=Nsupply_PROTEVS.Nsupply_ini';
% % for ilat=1:length(Nsupply_PROTEVS.lat)
% %     for ilon=1:length(Nsupply_PROTEVS.lon)
% %     init.Nsupply(ilat,ilon)=interp1(Nsupply_PROTEVS.Nsupply_ini(ilat,ilon),time0); 
% %     end
% % end
% init.lat=Nsupply_PROTEVS.lat; 
% init.lon=Nsupply_PROTEVS.lon;
% % for ilat=1:length(init.lat)
% % 	icoast=ga_find_index(coast_y,Nsupply.lat(ilat)); 
% % 	init.lon(ilat)=min(interp1(1:length(coast_x),coast_x,icoast)); 
% % end
% 
% % Run the growth-advection program
% zoo=ga_growthadvection(init,name_curr,datenum(2018,4,30),'options_plankton_model',options_plankton_model);
% 
% % Figure
% % Note: pixels over land are due to current interpolation to the coast (so pixels overlay land). 
% % The functions that concatenes daily runs to generate maps moves them over to the coastline again (see ga_concatene_runs)
% figure, hold on
% 	scatter(flipud(zoo.lon2D(:)),flipud(zoo.lat2D(:)),5,flipud(zoo.Z_big(:)),'filled')
% 	xlim([0 7]), ylim([36 40])
% 	hbar=colorbar; 
%     %caxis([0 20])
% 	set(get(hbar,'title'),'string',zoo.units.Z_big);
% 	xlabel('Longitude'), ylabel('Latitude')
% 	title('Z\_big from GA run initialized on May 1st, 2008')
% print('-djpeg','-r300','outputs/GArun_start_20080501.jpg')
% 
% %% 
%% -------------- Run the full GA model -------------- %%

%%%Load inputs and set run options
load('inputs/Nsupply_PROTEVS.mat')				% load the Nsupply forcing for the 2008 season (Nsupply.name = CCMP3km)
options_plankton_model={'gmax_big',0.432,'eZ',0.1,'mZ',0.005};	% krill parameterization
name_curr='PROTEVS_bio2';									% using currents toolbox_* into Ariane_workplace/currents_data

% Run the full GA model
ga_full_GArun(Nsupply_PROTEVS,name_curr,'options_plankton_model',options_plankton_model)

%%
load('outputs/zoo_Lagrangian')
load('outputs/zoo')

reso = curr.lon(2)-curr.lon(1);
xvalue_curr = min(curr.lon):reso:max(curr.lon);
yvalue_curr = min(curr.lat):reso:max(curr.lat);
[X_curr, Y_curr] = meshgrid(xvalue_curr, yvalue_curr);
Lat = zoo.lat2D;
Lon = zoo.lon2D;

%%%Pico
%convert modeled biomasse mmol C/m3 into abundance (cell/cm3)
QC_pico = 0.26*exp(-5.8702)*(0.9*10000)^(0.9228)*0.86*1000;
QC_pico = QC_pico*1E-12/12.106;

zoo_ab = struct();
zoo_ab.P_small = (zoo_all.P_small./QC_pico)./1000000; %convert biomasse mmol C/m3 into abundance (cell/cm3)


%convert in situ abundances cell/cm³ into biomasse (mmol C/m3)
pico_ab = dataCYTONEW(407:511,10);
pico_ab = pico_ab.*1000000; %cell/m3
pico_biom = pico_ab.*QC_pico;

Data = zoo_all.P_small(:,end-6:end);
n_array = 2;
lat_edge = min(Lat):0.025*n_array:max(Lat);
lon_edge = min(Lon):0.025*n_array:max(Lon);
meanData = histcn([Lat(:,end) Lon(:,end)], lat_edge, lon_edge, 'AccumData', Data, 'Fun', @mean);
% NaN_column = NaN(size(meanData,1),1);
% meanData = [NaN_column meanData];
% meanData(size(meanData,1)+1,:) = NaN;
meanData(meanData==0)=NaN;
pico = struct();
pico.biom_mod = meanData;
pico.lon_mod = lon_edge;
pico.lat_mod = lat_edge;
pico.biom_situ = pico_biom;
pico.lon_situ = lon_abundance(407:511);
pico.lat_situ = lat_abundance(407:511);
save('outputs/pico_struc','pico');

figure('DefaultAxesFontSize',22)
m_proj('mercator','lon',[0 7],'lat',[36 40]);
m_pcolor(lon_edge(1:size(meanData,2)),lat_edge(1:size(meanData,1)),meanData)
shading flat
hold on
%m_scatter(lon_abundance(407:511),lat_abundance(407:511),30,pico_biom,'filled')%10=pico, 14=micro
m_quiver(X_curr,Y_curr,mean(u0(:,:,11:13),3),mean(v0(:,:,11:13),3),3,'r');
m_usercoast('gumby','patch','w');
m_grid('box','fancy','linestyle','-','gridcolor','none','backcolor','none');
hold on
cbar = colorbar;
colorTitleHandle = get(cbar,'Title');
titleString = ({'Biomasse (mmol C.m⁻³)'});
set(colorTitleHandle ,'String',titleString);

%%% Micro
%convert modeled biomasse mmol C/m3 into abundance (cell/cm3)
QC_micro = 0.26*exp(-5.8702)*(90*10000)^(0.9228)*0.86*1000;
QC_micro = QC_micro*1E-12/12.106;

zoo_ab = struct();
zoo_ab.P_big = (zoo_all.P_big./QC_micro)./1000000; %convert biomasse mmol C/m3 into abundance (cell/cm3)


%convert in situ abundances cell/cm³ into biomasse (mmol C/m3)
micro_ab = dataCYTONEW(407:511,14);
micro_ab = micro_ab.*1000000; %cell/m3
micro_biom = micro_ab.*QC_micro;

Data = zoo_all.P_big(:,end-6:end);
n_array = 2;
lat_edge = min(Lat):0.025*n_array:max(Lat);
lon_edge = min(Lon):0.025*n_array:max(Lon);
meanData = histcn([Lat(:,end) Lon(:,end)], lat_edge, lon_edge, 'AccumData', Data, 'Fun', @mean);
% NaN_column = NaN(size(meanData,1),1);
% meanData = [NaN_column meanData];
% meanData(size(meanData,1)+1,:) = NaN;
meanData(meanData==0)=NaN;
micro = struct();
micro.biom_mod = meanData;
micro.lon_mod = lon_edge;
micro.lat_mod = lat_edge;
micro.biom_situ = micro_biom;
micro.lon_situ = lon_abundance(407:511);
micro.lat_situ = lat_abundance(407:511);
save('outputs/micro_struc','micro');
disp('start_GA_toolbox_test : done');
toc