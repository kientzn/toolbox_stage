%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% Display correlation for modeled and in situ biomasses%%%%%%%%%%%%%%%
%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%%%%% N. KIENTZ, 2022 %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
%%
tic
%clear all;
%close all;
addpath(genpath('utils'))
addpath(genpath('outputs'))
addpath(genpath('Ariane_worplace'))
addpath(genpath('CHL'))

run('transfo_currents.m');
run('chl_map.m');
run('sst_map.m');
run('abundance_map.m');
run('Abundance_relations.m');
run('relation_T_climato_T_tsg.m');
run('test_ini_map.m');

load('outputs/zoo_Lagrangian')

%% --------------------------------------------- Set up ----------------------------------------------- %%

% Set directory where Ariane is installed (used in most functions)
% There must be a directory "currents_data" inside dir_ariane_global where currents netcdf files are saved (see ga_write_ariane_currents)
global dir_ariane_global
dir_ariane_global='Ariane_workplace/';

% Set directory where outputs will be saved (used in ga_full_GArun)
global dir_output_global
dir_output_global='outputs/';

% Note - m_map can be used to visualize output maps (https://www.eoas.ubc.ca/~rich/map.html).

%%%%%%%%% GENERATE 1 PARTICULE FOR EACH GRID MESH %%%%%%%%%%%%%%%%%%%%%%%%
xvalue = min(lon_MUR):0.01:max(lon_MUR);
yvalue = min(lat_MUR):0.01:max(lat_MUR);
[X,Y] = meshgrid(xvalue, yvalue);
point_1 = [X(:) Y(:)];

reso = curr.lon(2)-curr.lon(1); %resolution
xvalue_curr = min(curr.lon):reso:max(curr.lon);
yvalue_curr = min(curr.lat):reso:max(curr.lat);
[X_curr,Y_curr] = meshgrid(xvalue_curr, yvalue_curr);

%If ga_advection_ariane does not work, try first to run "ariane" from a terminal inside Ariane_workplace.

iok = ~isnan(mat_micro);
mat_positions_ini=[X(iok) Y(iok)];

positions=ga_advection_ariane(mat_positions_ini,'PROTEVS_','dt',0.2,'time0',time(120),'nbdays_advec',13);

%%% Micro
mat_micro = mat_micro(iok);
abundance_matrix_micro_extp = reshape(mat_micro,[size(mat_micro,1)*size(mat_micro,2),1]);
for it = 1:length(positions.time)-1
    abundance_matrix_micro_extp = [abundance_matrix_micro_extp reshape(mat_micro,[size(mat_micro,1)*size(mat_micro,2),1])];
end

%%% Pico
mat_pico = mat_pico(iok);
abundance_matrix_pico_extp = reshape(mat_pico,[size(mat_pico,1)*size(mat_pico,2),1]);
for it = 1:length(positions.time)-1
    abundance_matrix_pico_extp = [abundance_matrix_pico_extp reshape(mat_pico,[size(mat_pico,1)*size(mat_pico,2),1])];
end

%% Correlation for modeled abundance
Lat = positions.lat2D(:,end-15:end);
Lon = positions.lon2D(:,end-15:end);

Data = (abundance_matrix_pico_extp(:,end-15:end)*1E6)*(((0.26*(exp(-5.87020)*(0.9*1E4)^0.92280)*0.86)*1000)*1E-12/12.106);
n_array = 6;
lat_edge = min(lat_MUR)+(0.01*n_array)/2:0.01*n_array:max(lat_MUR)+(0.01*n_array)/2;
lon_edge = min(lon_MUR)+(0.01*n_array)/2:0.01*n_array:max(lon_MUR)+(0.01*n_array)/2;
meanData = histcn([Lat(:) Lon(:)], lat_edge, lon_edge, 'AccumData', Data, 'Fun', @mean);

ab_adv = [];
for i = 1:105 %Number of measurements in the hippodrome
 [d, ix ] = min( abs( dataCYTONEW(406+i, end-1)-lon_edge ) );        
        lon_index_ab(it)=ix;%indice des latitudes
 [d, iy ] = min( abs( dataCYTONEW(406+i,end)-lat_edge ) );        
        lat_index_ab(it)=iy;%indice des latitudes
ab_adv(i) = meanData(iy,ix);
end

ab_cyto = (dataCYTONEW(407:511,10)*1E6)*(((0.26*(exp(-5.87020)*(0.9*1E4)^0.92280)*0.86)*1000)*1E-12/12.106); %14=micro %10=pico

%%%coloration if N or S of the front
N_part = [];
S_part = [];
for i = 1:length(ab_cyto)
    if dataCYTONEW(406+i,end) > 38.45
        N_part(length(N_part)+1)=i;
    elseif dataCYTONEW(406+i,end) <=38.45
        S_part(length(S_part)+1)= i;
    end
end
X = min(ab_cyto):0.001:max(ab_cyto);
Y = X;
mdl = fitlm(ab_adv,ab_cyto);
%
figure('DefaultAxesFontSize',22), hold on
plotAdded(mdl)
scatter(ab_adv(N_part),ab_cyto(N_part),35,'r','filled','DisplayName','Partie N du front')
scatter(ab_adv(S_part),ab_cyto(S_part),35,'k','filled','DisplayName','Partie S du front')
plot(X,Y,'--b','DisplayName','y = x','LineWidth',2)
xlim([min(ab_adv) max(ab_adv)]);
xlabel('Biomasse advectée (mmol C.m⁻³)');
ylabel('Biomasse in situ (mmol C.m⁻³)');
r2 = mdl.Rsquared.ordinary;
title('Pico R² = ', r2)

%%% Correlation for biomasse modelled and advected
%% Pico
load('outputs/pico_struc');

ab_adv = [];
ab_cyto = dataCYTONEW(407:511, 10); %14=micro %10=pico

pico_lon_interp = min(pico.lon_mod):0.01:max(pico.lon_mod);
pico_lat_interp = min(pico.lat_mod):0.01:max(pico.lat_mod);
[Xq, Yq] = meshgrid(pico_lon_interp,pico_lat_interp);
pico_interp = interp2(pico.lon_mod(1:end-1),pico.lat_mod(1:end-1),pico.biom_mod,Xq, Yq);

for i = 1:105
 [d, ix ] = min( abs( pico.lon_situ(i)-pico_lon_interp ) );        
        lon_index_ab(i)=ix;%indice des latitudes
 [d, iy ] = min( abs( pico.lat_situ(i)-pico_lat_interp ) );        
        lat_index_ab(i)=iy;%indice des latitudes
ab_adv(i) = pico_interp(iy,ix);
time_hippo(i)=datenum(dataCYTONEW(406+i,1),dataCYTONEW(406+i,2),dataCYTONEW(406+i,3),dataCYTONEW(406+i,4),dataCYTONEW(406+i,5),dataCYTONEW(406+i,6));
end

mdl = fitlm(ab_adv,pico.biom_situ);
ab_cyto = (dataCYTONEW(407:511,10)*1E6)*(((0.26*(exp(-5.87020)*(0.9*1E4)^0.92280)*0.86)*1000)*1E-12/12.106); %14=micro %10=pico

X = min(ab_cyto):0.001:max(ab_cyto);
Y = X;
%%%coloration if N or S of the front
N_part = [];
S_part = [];
for i = 1:length(ab_cyto)
    if pico.lat_situ(i) > 38.45
        N_part(length(N_part)+1)=i;
    elseif pico.lat_situ(i) <=38.45
        S_part(length(S_part)+1)= i;
    end
end
figure('DefaultAxesFontSize',22), hold on
plotAdded(mdl)
scatter(ab_adv(N_part),pico.biom_situ(N_part),35,'r','filled','DisplayName','Partie N du front')
scatter(ab_adv(S_part),pico.biom_situ(S_part),35,'k','filled','DisplayName','Partie S du front')
plot(X,Y,'--b','DisplayName','y = x','LineWidth',2)
xlim([min(ab_adv) max(ab_adv)]);
xlabel('Biomasse modélisée (cell.cm⁻³)');
ylabel('Biomasse in situ (cell.cm⁻³)');
r2 = mdl.Rsquared.ordinary;
title('Pico R² = ', r2)

for i = 1:105
    err(i) = cell2mat(table2cell(mdl.Residuals(i,1)));
end
% figure('DefaultAxesFontSize',22), hold on
% plotAdded(mdl)
% plot(X,Y,'--b','DisplayName','y = x','LineWidth',2)
% errorbar(ab_adv,pico.biom_situ,err,'o','DisplayName','\Delta ab')
% scatter(ab_adv,pico.biom_situ,35,time_hippo,'filled','DisplayName','Data')
% colorbar
% cbdate('yyyy-mm-dd')
% xlim([min(ab_adv) max(ab_adv)]);
% xlabel('Biomasse modélisée (cell.cm⁻³)');
% ylabel('Biomasse in situ (cell.cm⁻³)');
% r2 = mdl.Rsquared.ordinary;
% title('Pico R² = ', r2)

%% Micro
load('outputs/micro_struc');


micro_lon_interp = min(micro.lon_mod):0.01:max(micro.lon_mod);
micro_lat_interp = min(micro.lat_mod):0.01:max(micro.lat_mod);  
[Xq, Yq] = meshgrid(micro_lon_interp,micro_lat_interp);
micro_interp = interp2(micro.lon_mod(1:end-1),micro.lat_mod(1:end-1),micro.biom_mod,Xq, Yq);

ab_adv = [];
for i = 1:105
 [d, ix ] = min( abs( micro.lon_situ(i)-micro_lon_interp ) );        
        lon_index_ab(i)=ix;%indice des latitudes
 [d, iy ] = min( abs( micro.lat_situ(i)-micro_lat_interp ) );        
        lat_index_ab(i)=iy;%indice des latitudes
ab_adv(i) = micro.biom_interp(iy,ix);
end

mdl = fitlm(ab_adv,micro.biom_situ);
ab_cyto = (dataCYTONEW(407:511,14)*1E6)*(((0.26*(exp(-5.87020)*(90*1E4)^0.92280)*0.86)*1000)*1E-12/12.106); %14=micro %10=pico

X = min(ab_cyto):0.001:max(ab_cyto);
Y = X;
%%%coloration if N or S of the front
N_part = [];
S_part = [];
for i = 1:length(ab_cyto)
    if micro.lat_situ(i) > 38.45
        N_part(length(N_part)+1)=i;
    elseif micro.lat_situ(i) <=38.45
        S_part(length(S_part)+1)= i;
    end
end
figure('DefaultAxesFontSize',22), hold on
plotAdded(mdl)
scatter(ab_adv(N_part),micro.biom_situ(N_part),35,'r','filled','DisplayName','Partie N du front')
scatter(ab_adv(S_part),micro.biom_situ(S_part),35,'k','filled','DisplayName','Partie S du front')
plot(X,Y,'--b','DisplayName','y = x','LineWidth',2)
xlim([min(ab_adv) max(ab_adv)]);
xlabel('Biomasse modélisée (cell.cm⁻³)');
ylabel('Biomasse in situ (cell.cm⁻³)');
r2 = mdl.Rsquared.ordinary;
title('Micro R² = ', r2)
toc